<?php
/**
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/**
 * Theme's functions.php file.
 *
 * This file bootstrap the entire framework.
 *
 * @package Yithemes
 *
 */


/*
 * WARNING: This file is part of the Your Inspiration Themes framework core.
 * Edit this section at your own risk.
 */

//let's start the game!
require_once('core/yit.php');


function wpg_add_columns($cols) {

        $cols['wc_settings_tab_customer_address'] = __( 'Delivery Address', 'mytheme' );
	return $cols;
}
add_filter('wpg_order_columns', 'wpg_add_columns');


function wpg_add_fields($settings) {

	$settings['customer_address'] = array(
  'name' => __( 'Delivery Address', 'woocommerce-simply-order-export' ),
  'type' => 'checkbox',
  'desc' => __( 'Delivery Address', 'woocommerce-simply-order-export' ),
  'id' => 'wc_settings_tab_customer_address'
);

	return $settings;

}
add_filter('wc_settings_tab_order_export', 'wpg_add_fields');


function csv_write( &$csv, $od, $fields ) {

	if( !empty( $fields['wc_settings_tab_customer_address'] ) && $fields['wc_settings_tab_customer_address'] === true ){
$customer_address = $od->get_formatted_billing_address();
  array_push( $csv, $customer_address );
};
	

}
add_action('wpg_before_csv_write', 'csv_write', 10, 3);








